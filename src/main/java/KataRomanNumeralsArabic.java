


import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class KataRomanNumeralsArabic {
		private static Map<Character, Integer> numbers = new HashMap<>(7);
		static {
			numbers.put('I', 1);
			numbers.put('V', 5);
			numbers.put('X', 10);
			numbers.put('L', 50);
			numbers.put('C', 100);
			numbers.put('D', 500);
			numbers.put('M', 1000);
		}

		public int romanToInt(String string) {
			int sum = 0;
			int length = string.length() - 1;
			for (int i = 0; i < length; i++) {
				if (numbers.get(string.charAt(i)) < numbers.get(string.charAt(i + 1))) {
					sum -= numbers.get(string.charAt(i));
				} else {
					sum += numbers.get(string.charAt(i));
				}
			}
			return sum += numbers.get(string.charAt(length));
		}
	}