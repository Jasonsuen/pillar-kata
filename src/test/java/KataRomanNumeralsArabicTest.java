

import org.junit.*;
import static org.junit.Assert.*;

import java.util.TreeMap;

public class KataRomanNumeralsArabicTest {
	/*1 ---> I
	10 --> X
	7 ---> VII
	*/
	
	private KataRomanNumeralsArabic roman;
	
	@Before
	public void setup() {
	    roman = new KataRomanNumeralsArabic();
	}

	@Test
	public void given_I_return_1() {
	    int results = roman.romanToInt("I");
	    
	    Assert.assertEquals(1, results);

	}
	@Test
	public void given_III_return_3() {
	    int results = roman.romanToInt("III");
	    
	    Assert.assertEquals(3, results);

	}
	@Test
	public void given_IX_return_9() {
	    int results = roman.romanToInt("IX");
	    
	    Assert.assertEquals(9, results);

	}
	@Test
	public void given_MLXVI_return_1066() {
	    int results = roman.romanToInt("MLXVI");
	    
	    Assert.assertEquals(1066, results);

	}
	@Test
	public void given_MCMLXXXIX_return_1989() {
	    int results = roman.romanToInt("MCMLXXXIX");
	    
	    Assert.assertEquals(1989, results);

	}
}

