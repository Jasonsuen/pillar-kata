

import org.junit.*;




public class KataRomanNumeralsTest {
	/*1 ---> I
	10 --> X
	7 ---> VII
	*/
	
	private KataRomanNumerals roman;
	
	@Before
	public void setup() {
	    roman = new KataRomanNumerals();
	}

	@Test
	public void given_1_return_I() {
	    String results = roman.intoRomanNumbers(1);
	    
	    Assert.assertEquals("I", results);

	}
	@Test
	public void given_3_return_III() {
	    String results = roman.intoRomanNumbers(3);
	    
	    Assert.assertEquals("III", results);

	}
	@Test
	public void given_9_return_IX() {
	    String results = roman.intoRomanNumbers(9);
	    
	    Assert.assertEquals("IX", results);

	}
	@Test
	public void given_1066_return_MLXVI() {
	    String results = roman.intoRomanNumbers(1066);
	    
	    Assert.assertEquals("MLXVI", results);

	}
	@Test
	public void given_1989_return_MCMLXXXIX() {
	    String results = roman.intoRomanNumbers(1989);
	    
	    Assert.assertEquals("MCMLXXXIX", results);

	}
}
